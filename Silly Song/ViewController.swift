//
//  ViewController.swift
//  Silly Song
//
//  Created by Edwin Albert on 10/1/18.
//  Copyright © 2018 Edwin Albert. All rights reserved.
//

import UIKit

let bananaFanaTemplate = [
    "<FULL_NAME>, <FULL_NAME>, Bo B<SHORT_NAME>",
    "Banana Fana Fo F<SHORT_NAME>",
    "Me My Mo M<SHORT_NAME>",
    "<FULL_NAME>"].joined(separator: "\n")

func customizeTemplate(_ name : String,_ Template: String){
    
}

func shortNameFromName(name : String)->String{
    let lowercaseName = name.lowercased()
    let vowerSet = CharacterSet(charactersIn: "aiueo")
    
    let range = lowercaseName.rangeOfCharacter(from: vowerSet)
    //    let result2 = String(lowercaseName[range!])
    if range?.isEmpty ?? true {
        return ""
    }
    let startIndex = range!.lowerBound
    let result = String(lowercaseName[startIndex...])
    
    return result
}

func lyricsForName(_ lyricsTemplate: String,_ fullName: String) -> String{
    let shortName = shortNameFromName(name: fullName)
    if shortName.isEmpty {
        return "Must have vocal letters!"
    }
    let lyrics = lyricsTemplate
        .replacingOccurrences(of: "<FULL_NAME>", with: fullName)
        .replacingOccurrences(of: "<SHORT_NAME>", with: shortName)
    
    return lyrics
}

func notAllowed(_ string : String) -> Bool{
//    if string.rangeOfCharacter(from: NSCharacterSet.decimalDigits) != nil || string.rangeOfCharacter(from: NSCharacterSet.symbols) != nil {
    if string.rangeOfCharacter(from: NSCharacterSet.letters.inverted) != nil {
        return true
    }
    else {
        return false
    }
}

class ViewController: UIViewController {
    
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var lyricsView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        nameField.delegate = self
        self.view.backgroundColor = UIColor(patternImage: (UIImage(named: "Image"))!)

    }
    
    @IBAction func reset(_ sender: Any) {
        if(nameField.text == ""){
            lyricsView.text="Your Song Here"
        }
        else{
            nameField.text = ""
            lyricsView.text="Your Song Here"
        }
    }
    
    @IBAction func displayLyrics(_ sender: Any) {
        if nameField.text?.isEmpty ?? true {
            print("Name Field is empty")
        } else {
            if notAllowed(nameField.text!) == true{
                lyricsView.text = "Only letters allowed!"
            }
            else{
                let name = nameField.text
                let lyrics = lyricsForName(bananaFanaTemplate, name!)
                lyricsView.text = ""
                lyricsView.insertText(lyrics)
            }
        }
        
    }
    
    
}

// MARK: - UITextFieldDelegate
extension ViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
}

extension String  {
    var isNumber: Bool {
        return !isEmpty && rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil
    }
}
