import UIKit

let bananaFanaTemplate = [
    "<FULL_NAME>, <FULL_NAME>, Bo B<SHORT_NAME>",
    "Banana Fana Fo F<SHORT_NAME>",
    "Me My Mo M<SHORT_NAME>",
    "<FULL_NAME>"].joined(separator: "\n")

func customizeTemplate(_ name : String,_ Template: String){
    
}

func shortNameFromName(name : String)->String{
    let lowercaseName = name.lowercased()
    let vowerSet = CharacterSet(charactersIn: "aiueo")
    
    let range = lowercaseName.rangeOfCharacter(from: vowerSet)
//    let result2 = String(lowercaseName[range!])
    let startIndex = range!.lowerBound
    let result = String(lowercaseName[startIndex...])
    
    return result
}

func lyricsForName(_ lyricsTemplate: String,_ fullName: String) -> String{
    let shortName = shortNameFromName(name: fullName)
    
    let lyrics = lyricsTemplate
        .replacingOccurrences(of: "<FULL_NAME>", with: fullName)
        .replacingOccurrences(of: "<SHORT_NAME>", with: shortName)
    
    return lyrics
}

lyricsForName(bananaFanaTemplate, "Nate")
